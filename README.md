# gradle_project



## Getting started

## command line instructions to setup gradle project

To initialize a Gradle project -> gradle init

  -- project to generate: application

  -- implementation language: Java

  -- build script DSL: Groovy

  -- generate build using new APIs and behavior: press enter

  -- Select test framework: TestNG/Junit

  -- project name: desired name

  -- source package: desired name


settings.gradle- During initialization, Gradle reads the settings.gradle file to figure out which projects are to take part in a build.

gradlew - is for gradle wrapper which can let your project independent with the environment

## command to execute task using gradle wrapper
 --./gradlew personalInfo

## command to execute task using groovy 
 -- groovy PersonalInfo.groovy